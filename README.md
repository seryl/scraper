scraper
=======

This library is designed to be a `general purpose scraper` with callbacks; the entire intent
is that this is supposed to be wrapped and consumed by anything downstream.

Currently the following behaviors are configured

* Queue and scraper timings
* Multiple threaded workers
* Callback hooks
* Request ledger with optional removal

Everything is returned as a goquery parsed document and you can append child scrapes into
the same job, just so you can have a parent that has all of the pages associated.

If you go this multi-parent route, you may wish to clear both `Document` and `Unparsed` entries
after any mid-flight information has been stored.
