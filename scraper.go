package scraper

import (
	"net/http"
	"sync"
	"time"

	uuid "github.com/satori/go.uuid"
)

// Server is the scraper server for handling multiple http scraping requests.
type Server struct {
	sync.Mutex
	wg *sync.WaitGroup

	Client  *http.Client
	Config  *Config
	Workers []*Worker

	requestQueue chan *Request
	requestList  map[string]*Request
}

// Start will start the worker pool for the scraper.
func (s *Server) Start() {
	for i := 0; i < s.Config.Workers; i++ {
		worker := &Worker{
			wg:           &sync.WaitGroup{},
			Client:       s.Client,
			requestQueue: s.requestQueue,
		}
		s.Workers = append(s.Workers, worker)
		s.wg.Add(1)
		worker.Start()
	}
}

// Stop will stop force workers to quit after their current requests.
// TODO: We can/should probably drain the remaining request pool afterwards.
func (s *Server) Stop() {
	for _, worker := range s.Workers {
		go func(wrk *Worker, wg *sync.WaitGroup) { wrk.Stop(); wg.Done() }(worker, s.wg)
	}
	s.wg.Wait()
}

// New returns a scraper configured with sane defaults.
// TODO: We may want to consider storing scrapers by tag as well.
func New(cfg *Config) *Server {
	reqQueue := make(chan *Request, cfg.QueueSize)
	tr := &http.Transport{MaxIdleConnsPerHost: cfg.IdleConnections}
	client := &http.Client{Transport: tr}

	return &Server{
		wg: &sync.WaitGroup{},

		Client: client,
		Config: cfg,

		requestQueue: reqQueue,
		requestList:  make(map[string]*Request),
	}
}

// AddRequest appends a new request onto the scraper.
func (s *Server) AddRequest(req *Request) {
	s.Lock()
	s.requestList[req.ID.String()] = req
	s.Unlock()
	req.Queued = time.Now()
	req.Scraper = s
	s.requestQueue <- req
}

// RemoveRequest removes a parent request from the RequestList
// Note: This is typically used to cleanup jobs on completion.
func (s *Server) RemoveRequest(reqID uuid.UUID) {
	s.Lock()
	delete(s.requestList, reqID.String())
	s.Unlock()
}

// RequestList is the mapping of request IDs and the request objects themselves.
func (s *Server) RequestList() map[string]*Request {
	s.Lock()
	defer s.Unlock()
	return s.requestList
}
