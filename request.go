package scraper

import (
	"net/http"
	"time"

	"github.com/PuerkitoBio/goquery"
	uuid "github.com/satori/go.uuid"
)

// RequestFunc is the function handler for a single request.
type RequestFunc func(req *Request) error

// CompletedFunc is the function handler for callbacks on success.
type CompletedFunc func(req *Request)

// ErrorFunc is the function handler for failed request attempts.
type ErrorFunc func(req *Request, err error)

// RequestCallbacks is the set of callback functions associated with a request.
type RequestCallbacks struct {
	Handle   RequestFunc
	Complete CompletedFunc
	Error    ErrorFunc
}

// Request is a single job request, which may contain multiple
// child requests and a single parent.
//
// Note: All of these requests, especially when adding child requests,
// should be done sequentially. Otherwise the total number of outgoing
// connections will get out of hand very quickly.
type Request struct {
	// Used for adding child requests
	Scraper *Server

	Queued   time.Time
	Started  time.Time
	Finished time.Time

	Tag string
	ID  uuid.UUID

	Parent   *Request
	Children map[string]*Request

	URL       string
	Callbacks *RequestCallbacks
	Document  *goquery.Document

	// Note: this is only used when initial parse fails
	Unparsed *http.Response
}

// Start attempts to parse the target endpoint.
// If there are errors detected, the request returns the unparsed response.
func (r *Request) Start(client *http.Client) error {
	var (
		doc  *goquery.Document
		resp *http.Response
	)

	req, err := http.NewRequest("GET", r.URL, nil)
	if err != nil {
		goto request_failed
	}

	r.Started = time.Now()
	resp, err = client.Do(req)
	if err != nil {
		goto request_failed
	}

	doc, err = goquery.NewDocumentFromResponse(resp)
	if err != nil {
		goto request_failed
	}

	r.Document = doc
	return nil

request_failed:
	r.Unparsed = resp
	r.Finished = time.Now()
	return err
}

// TimeQueued is the total time spent in the queue
func (r *Request) TimeQueued() time.Duration {
	return r.Started.Sub(r.Queued)
}

// TotalElapsed is the total time including the queued time.
func (r *Request) TotalElapsed() time.Duration {
	return r.Finished.Sub(r.Queued)
}

// TimeScraping is the total amount of time the request was scraping
func (r *Request) TimeScraping() time.Duration {
	return r.Finished.Sub(r.Started)
}

// NewRequest returns a new scraper request.
func NewRequest(url string, callbacks RequestCallbacks, tag string) *Request {
	return &Request{
		Tag:       tag,
		ID:        uuid.NewV4(),
		URL:       url,
		Callbacks: &callbacks,
	}
}
