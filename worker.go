package scraper

import (
	"net/http"
	"sync"
	"time"
)

// Worker is a request handler which listens for exit signals.
type Worker struct {
	sync.Mutex
	wg           *sync.WaitGroup
	Client       *http.Client
	requestQueue <-chan *Request
	quit         chan struct{}
}

// Start begins the Worker on a goroutine.
func (w *Worker) Start() {
	w.Lock()
	w.quit = make(chan struct{}, 1)
	w.wg.Add(1)
	go w.Work()
	w.Unlock()
}

// Stop tells the worker to exit.
func (w *Worker) Stop() {
	w.Lock()
	close(w.quit)
	w.wg.Wait()
	w.Unlock()
}

// Work listens on the requests queue until told to exit.
func (w *Worker) Work() {
	var err error

	for {
		select {
		case <-w.quit:
			w.wg.Done()
			return
		case req, ok := <-w.requestQueue:
			if ok {
				err = req.Start(w.Client)
				if err != nil {
					req.Finished = time.Now()
					req.Callbacks.Error(req, err)
					continue
				}

				err = req.Callbacks.Handle(req)
				if err != nil {
					req.Finished = time.Now()
					req.Callbacks.Error(req, err)
					continue
				}

				req.Finished = time.Now()
				req.Callbacks.Complete(req)
			}
		}
	}
}
