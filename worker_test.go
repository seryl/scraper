package scraper

import (
	"net/http"
	"strings"
	"sync"
	"testing"
	"time"

	"github.com/PuerkitoBio/goquery"
)

func TestScrapingJavascript(t *testing.T) {
	failed := false
	found := false
	finished := make(chan struct{}, 1)
	requestQueue := make(chan *Request, 1)
	wg := &sync.WaitGroup{}

	callbacks := RequestCallbacks{
		Handle: func(req *Request) error {
			req.Document.Find("script").Each(func(i int, s *goquery.Selection) {
				if strings.Contains("var sherlock_firstbyte", s.Text()) {
					found = true
				}
			})
			if !found {
				t.Fatal("Unable to parse sherlock_firstbyte from airbnb script text")
			}

			finished <- struct{}{}
			return nil
		},
		Error: func(req *Request, err error) {
			if err != nil {
				t.Fatalf("Javacript scraper failed with error: %s", err)
			}
		},
		Complete: func(req *Request) {},
	}

	worker := &Worker{
		wg:           &sync.WaitGroup{},
		Client:       http.DefaultClient,
		requestQueue: requestQueue,
	}

	worker.Start()

	req := NewRequest("https://airbnb.com", callbacks, "")
	requestQueue <- req

	wg.Add(1)
	timeout := time.After(10 * time.Second)
	go func() {
		select {
		case <-timeout:
			failed = true
			wg.Done()
		case <-finished:
			worker.Stop()
			wg.Done()
		}
	}()

	wg.Wait()
	if failed {
		t.Fatal("Javascript scraper timed out during test scrape for airbnb")
	}
}
