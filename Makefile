APPNAME=scraper
VERBOSE?=false
TEST?=$(shell go list ./... | grep -v /vendor/ | xargs -L1)
VETARGS?= -asmdec1 -atomic -bool -buildtags -copylocks -methods -nilfunc -printf -rangeloops -shift -structtags -unsafeptr
EXTERNAL_TOOLS=\
	github.com/Masterminds/glide \
	github.com/golang/lint/golint \
	golang.org/x/tools/cmd/cover

default: test

all: test check

# test runs the unit tests and vets the code
test:
	go test $(TEST) $(TESTARGS) -timeout=30s -parallel=4

# testv runs the unit tests verbosely and vets the code
testv: TESTARGS=-v
testv: test

# testacc runs acceptance tests
testacc: generate
	@if [ "$(TEST)" = "./..." ]; then \
		echo "ERROR: Set TEST to a specific package"; \
		exit 1; \
	fi
	go test $(TEST) -v $(TESTARGS) -timeout 45m

# testrace runs the race checker
testrace: generate
	go test -race $(TEST) $(TESTARGS)

cover:
	./scripts/coverage.sh --html

lint:
	golint ./...

# vet runs the Go source code static analysis tool `vet` to find
# any common errors.
vet:
	@go list -f '{{.Dir}}' ./... \
		| grep -v '*.bitbucket.org/seryl/scraper$$' \
		| xargs go tool vet ; if [ $$? -eq 1 ]; then \
			echo ""; \
			echo "Vet found suspicious constructs. Please check the reported constructs"; \
			echo "and fix them if necessary before submitting the code for reviewal."; \
		fi

# bootstrap the build by downloading additional tools
bootstrap:
	@for tool in $(EXTERNAL_TOOLS) ; do \
		echo "Installing $$tool" ; \
	go get $$tool; \
		done

watch:
	@echo 'Watching files for changes'
	@fswatch -r -l 1 -o -0 -e ./vendor ./ | xargs -0 -n 1 -I {} bash -c "make testv; echo"

.PHONY: default test testv testacc testrace cover lint vet bootstrap watch
