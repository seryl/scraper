package scraper

// Config is the configuration struct for the scraper.
type Config struct {
	QueueSize       int
	Workers         int
	IdleConnections int
	Timeout         int
	Retries         int
}

// DefaultConfig is the set of default configuration parameters for the scraper.
var DefaultConfig = &Config{
	QueueSize:       50000,
	Workers:         8,
	IdleConnections: 10,
	Timeout:         20,
	Retries:         3,
}
