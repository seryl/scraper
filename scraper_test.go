package scraper

import (
	"sync"
	"testing"
	"time"
)

func TestScraper(t *testing.T) {
	failed := false
	finished := make(chan struct{}, 1)
	wg := &sync.WaitGroup{}

	scraper := New(DefaultConfig)
	scraper.Start()

	callbacks := RequestCallbacks{
		Handle: func(req *Request) error {
			finished <- struct{}{}
			return nil
		},
		Error: func(req *Request, err error) {
			if err != nil {
				t.Fatalf("Server failed with error: %s", err)
			}
		},
		Complete: func(req *Request) {},
	}

	scraper.AddRequest(NewRequest("https://google.com", callbacks, ""))

	wg.Add(1)
	timeout := time.After(10 * time.Second)
	go func() {
		select {
		case <-timeout:
			failed = true
			wg.Done()
		case <-finished:
			scraper.Stop()
			wg.Done()
		}
	}()

	wg.Wait()
	if failed {
		t.Fatal("Server timed out during test scrape for google")
	}
}
